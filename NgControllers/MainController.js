(function() {
  var app = angular.module("githubViewer");

  var MainController = function(
        $scope, $log, $location) {
          
    $scope.search = function() {
      $log.info(Date() + ":Searching for " + $scope.username);
      
      $location.path("/user/" + $scope.username);
    }
    
  };

  app.controller("MainController", ["$scope", "$log", 
          "$location", MainController]);

}());