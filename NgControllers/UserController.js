(function() {
  var emptyData = "Unknown";

  var app = angular.module("githubViewer");

  var UserController = function(
        $scope, github, $log, $routeParams) {

    var onUserComplete = function(data) {
      $scope.error = "";
      $scope.user = data;
      
      $scope.user.name = $scope.user.name || emptyData;
      $scope.user.location = $scope.user.location || emptyData;
      
      github.getRepos($scope.user)
          .then(onRepos, onError);
    };
    
    var onRepos = function(data) {
      $scope.repos = data;
      
      $log.info(Date() + ":Search finished for " + $scope.username);
    }

    var onError = function(reason) {
      $scope.error = "Error: Could not fetch the data.";
    }
    
    $scope.message = "AngularJS rocks!";
    $scope.username = $routeParams.username;
    $scope.repoSortOrder = "-stargazers_count";
    
    github.getUser($scope.username)
          .then(onUserComplete, onError);
  };

  app.controller("UserController", ["$scope", "github", "$log",
          "$routeParams", UserController]);

}());