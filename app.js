(function () {

    var app = angular.module("githubViewer", ["ngRoute"]);

    app.config(function ($routeProvider) {
        $routeProvider
    .when("/main", {
        templateUrl: "NgViews/main.html",
        controller: "MainController"
    })
    .when("/user/:username", {
        templateUrl: "NgViews/user.html",
        controller: "UserController"
    })
    .when("/repo/:username/:reponame", {
        templateUrl: "NgViews/repo.html",
        controller: "RepoController"
    })
    .otherwise({ redirectTo: "/main" });

    });

} ());


